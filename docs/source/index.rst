Welcome to XStatic's documentation!
===================================

What is XStatic
===============
.. toctree::
   :maxdepth: 2

   intro


Using XStatic
=============

.. toctree::
   :maxdepth: 2

   using
   packaging


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

